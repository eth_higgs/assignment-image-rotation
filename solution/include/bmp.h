#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include <stdint.h>

struct image_raw {
    uint64_t width, height;
    struct pixel *data;
};

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image_raw from_bmp(const char *filename);

void to_bmp(const char *filename, struct image_raw *img);

void image_raw_free(struct image_raw *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
