#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_ROTATOR_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_ROTATOR_H

#include "bmp.h"

struct image_raw rotate_image(const struct image_raw *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_ROTATOR_H
