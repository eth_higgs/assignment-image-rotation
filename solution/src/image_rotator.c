#include "../include/image_rotator.h"

#include <stdlib.h>


struct image_raw rotate_image(const struct image_raw *img) {
    uint64_t width = img->width, height = img->height;

    struct pixel* rotated_pixels = malloc(width * height * sizeof(struct pixel));

    for (size_t i = 0; i < height; i++) {
        for (size_t j = 0; j < width; j++) {
            struct pixel pixel = img->data[i * width + j];
            rotated_pixels[(height - 1 - i) + j * height] = pixel;
        }
    }

    struct image_raw rotated = {0};
    rotated.data = rotated_pixels;
    rotated.height = width;
    rotated.width = height;
    return rotated;
}
