#include "../include/bmp.h"

#include <stdio.h>
#include <stdlib.h>

#define HEADER_SIZE sizeof(struct bmp_header)
#define BMP_FILE_DEFAULT_TYPE 0x4d42
#define BMP_FILE_RESERVED 0
#define BI_HEADER_SIZE_40 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_WITHOUT_COMPRESSION 0
#define BI_X_PELS_PER_METER 2834
#define BI_Y_PELS_PER_METER 2834
#define BI_CLR_IMPORTANT 3

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct bmp_header bmp_header_generate(struct image_raw *image);

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

void image_raw_free(struct image_raw *img) {
    free(img->data);
}

struct bmp_header bmp_header_generate(struct image_raw *image) {
    struct bmp_header header = {0};
    uint32_t image_mem_size = ((-image->width) % 4 + image->width * 3 * sizeof(struct pixel)) * image->height;

    header.bfType = BMP_FILE_DEFAULT_TYPE;
    header.bfileSize = HEADER_SIZE + image_mem_size;
    header.bfReserved = BMP_FILE_RESERVED;
    header.bOffBits = HEADER_SIZE;
    header.biSize = BI_HEADER_SIZE_40;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BI_BIT_COUNT;
    header.biCompression = BI_WITHOUT_COMPRESSION;
    header.biSizeImage = image_mem_size;
    header.biXPelsPerMeter = BI_X_PELS_PER_METER;
    header.biYPelsPerMeter = BI_Y_PELS_PER_METER;
    header.biClrUsed = 3 * sizeof(struct pixel);
    header.biClrImportant = BI_CLR_IMPORTANT;

    return header;
}

#define HEADER_MEM_SIZE sizeof(struct bmp_header)
#define PIXEL_MEM_SIZE sizeof(struct pixel)

size_t count_read_padding(const uint32_t width) {
    return (4 - width * PIXEL_MEM_SIZE) % 4;
}

enum read_status load_bmp(FILE *in, struct image_raw *bmp);

struct image_raw from_bmp(const char *filename) {
    struct image_raw img = {0};

    FILE *in = fopen(filename, "rb");
    load_bmp(in, &img);
    fclose(in);

    return img;
}

void load_bmp_header(FILE *in, struct bmp_header *header);

void load_bmp_image(FILE *in, struct image_raw *image, const uint32_t width, const uint32_t height);

enum read_status load_bmp(FILE *in, struct image_raw *bmp) {
    struct bmp_header header = {0};

    load_bmp_header(in, &header);
    load_bmp_image(in, bmp, header.biWidth, header.biHeight);

    return READ_OK;
}

void load_bmp_header(FILE *in, struct bmp_header *header) {
    fread(header, 1, HEADER_MEM_SIZE, in);
}

void image_raw_init(struct image_raw *image, const uint32_t width, const uint32_t height) {
    image->width = width;
    image->height = height;
    image->data = malloc(width * height * PIXEL_MEM_SIZE);
}

void load_bmp_image(FILE *in, struct image_raw *image, const uint32_t width, const uint32_t height) {
    image_raw_init(image, width, height);

    const size_t padding = count_read_padding(width);

    for (size_t i = 0; i < height; i++) {
        fseek(in, HEADER_MEM_SIZE + i * (width * PIXEL_MEM_SIZE + padding), SEEK_SET);

        fread(&image->data[i * width], 1, PIXEL_MEM_SIZE * width, in);

        /*
        for (size_t j = 0; j < width; j++) {
            struct pixel pixel;
            fread(&pixel, 1, PIXEL_MEM_SIZE, in);
            image->data[i * width + j] = pixel;
        }
        */
    }
}

size_t count_write_padding(const uint64_t width) {
    return (4 - sizeof(struct pixel) * width) % 4;
}

enum write_status save_bmp(FILE *out, struct image_raw *img);

void to_bmp(const char *filename, struct image_raw *img) {
    FILE *out = fopen(filename, "wb");
    // if (!out) return WRITE_ERROR;

    save_bmp(out, img);

    fclose(out);
}

void save_bmp_header(FILE *out, struct bmp_header *header);

void save_bmp_image(FILE *out, struct image_raw *image);

enum write_status save_bmp(FILE *out, struct image_raw *img) {
    struct bmp_header header = bmp_header_generate(img);

    save_bmp_header(out, &header);
    save_bmp_image(out, img);

    return WRITE_OK;
}

void save_bmp_header(FILE *out, struct bmp_header *header) {
    fwrite(header, 1, sizeof(struct bmp_header), out);
}

void save_bmp_image(FILE *out, struct image_raw *image) {
    size_t padding = count_write_padding(image->width);
    char trash = 0;

    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            struct pixel pixel = image->data[i * image->width + j];
            fwrite(&pixel, 1, sizeof(struct pixel), out);
        }
        for (size_t j = 0; j < padding; j++) {
            fwrite(&trash, 1, sizeof(trash), out);
        }
    }
}
