#include "image_rotator.h"

int main(__attribute__((unused)) int argc, char **argv) {
    struct image_raw img = from_bmp(argv[1]);
    struct image_raw rotated_img = rotate_image(&img);
    image_raw_free(&img);
    to_bmp(argv[2], &rotated_img);
    image_raw_free(&rotated_img);
    return 0;
}
